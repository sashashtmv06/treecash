package com.sashashtmv.treecash;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.vk.sdk.VKSdk;

public class MyApp extends Application {
    private static final int VK_ID = 7238866;
    public static final String VK_API_VERSION = "5.52"; //current version
    private String googleAuthStringApiRes = "329490666380-nki8bbojgruatsg96bd2sr1j6rel80fg.apps.googleusercontent.com";

    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.customInitialize(this, VK_ID, VK_API_VERSION);
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.setApplicationId(getFacebookSdkStringApi());
//        googleAuthStringApiRes = getGoogleAuthStringApiRes();
    }

    public String getGoogleAuthStringApiRes() {
        return googleAuthStringApiRes;
    }

    public String getFacebookSdkStringApi(){
        return getString(R.string.facebook_app_id);
    }
}
