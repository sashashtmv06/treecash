package com.sashashtmv.treecash.model;

public class Order {
    private String date;
    private String id;
    private String storeName;
    private String sum;
    private String status;
    private String currency;

    public Order(String date, String id, String storeName, String sum, String status, String currency) {
        this.date = date;
        this.id = id;
        this.storeName = storeName;
        this.sum = sum;
        this.status = status;
        this.currency = currency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
