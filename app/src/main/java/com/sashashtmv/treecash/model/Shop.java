package com.sashashtmv.treecash.model;

import android.graphics.Bitmap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;



import static org.httprpc.beans.BeanAdapter.valueAt;

public class Shop {
    private String nameShop;
    private String shopUrl;
    private URL imageUrl;
    private List<String> cashback;
    private String categoryName;
    private int categoryId;
    private boolean love;

    public Shop(String nameShop, String shopUrl, URL imageUrl,List<String> cashback, String categoryName, int categoryId, boolean love) {
        this.nameShop = nameShop;
        this.shopUrl = shopUrl;
        this.imageUrl = imageUrl;
        this.cashback = cashback;
        this.categoryName = categoryName;
        this.categoryId = categoryId;
        this.love = love;
    }

    public void setNameShop(String nameShop) {
        this.nameShop = nameShop;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

    public void setImageUrl(URL imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setCashback(List<String> cashback) {
        this.cashback = cashback;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isLove() {
        return love;
    }

    public void setLove(boolean love) {
        this.love = love;
    }

    public String getNameShop() {
        return nameShop;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public URL getImageUrl() {
        return imageUrl;
    }

    public List<String> getCashback() {
        return cashback;
    }
}
