package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sashashtmv.treecash.MainActivity;
import com.sashashtmv.treecash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    private static Context mContext;
    private Button myOrders;
    private Button edit;
    private Button withdrawalOfFunds;
    private TextView changePassword;
    private TextView tvSum;
    private TextView tvTotal;
    private static String mSum;
    private static String mTotal;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(Context context, String sum, String total) {
        ProfileFragment fragment = new ProfileFragment();
        mContext = context;
        mSum = sum;
        mTotal = total;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        myOrders = view.findViewById(R.id.bt_my_orders);
        edit = view.findViewById(R.id.bt_edit);
        changePassword = view.findViewById(R.id.tv_change_password);
        withdrawalOfFunds = view.findViewById(R.id.bt_next);
        tvSum = view.findViewById(R.id.tv_sum);
        tvTotal = view.findViewById(R.id.tv_total);
        tvSum.setText(mSum);
        tvTotal.setText(mTotal);

        final MainActivity activity = (MainActivity)mContext;

        myOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onCreateMyOrdersFragment();
            }
        });

        withdrawalOfFunds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onCreateWithdrawalOfFundsFragment();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onCreateEditDataFragment();
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onCreateChangePasswordFragment();
            }
        });

        return view;
    }

}
