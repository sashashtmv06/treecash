package com.sashashtmv.treecash.fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


import com.google.android.material.navigation.NavigationView;
import com.sashashtmv.treecash.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.app.Fragment;

import static com.mikepenz.iconics.Iconics.TAG;


public class StartFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener, IOnBackPressed{
    Toolbar toolbar;
    private Callbacks mCallbacks;
    private Button jewelry;
    private Button beauty;
    private Button electric;
    private Button travel;
    private Button all_for_home;
    private Button entertainment;
    private Button food_delivery;
    private Button services;
    private Button finance;
    private Button closing;
    private Button china_shop;
    private Button foreign_shop;
    private Button all_shops;
    private View view;


    public StartFragment() {
        // Required empty public constructor
    }

    public interface Callbacks{
        void getBalance();
        void onCreateAllShopsFragment(String title);
        void onCreateDescribeFragment();
        void onCreateReferalProgramFragment();
        void onCreateRuleFragment();
        void onCreateAgreementFragment();
        void onCreateAskFragment();
        void onSupportFragment();
        void onCreateProfileFragment();
        void onCreateMyOrdersFragment();
        void onCreateChangePasswordFragment();
        void onCreateEditDataFragment();
        void onCreateWithdrawalOfFundsFragment();
        void onCreateEnterFragment();
        void onCreateReviewsFragment();
        void onCreateBlogFragment();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks)activity;
    }

    public static StartFragment newInstance() {
        StartFragment fragment = new StartFragment();

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.fragment_start, container, false);
       mCallbacks.getBalance();
        Log.i(TAG, "onCreateView: shadow - " + "true");
        setHasOptionsMenu(true);

        final DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.activity_main);

        ImageButton menuLeft = (ImageButton) view.findViewById(R.id.menuLeft);
        ImageButton menuRight = (ImageButton) view.findViewById(R.id.menuRight);

        menuLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });
        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/
        jewelry = view.findViewById(R.id.bt_jewelry);
        beauty = view.findViewById(R.id.bt_beauty);
        electric = view.findViewById(R.id.bt_electric);
        travel = view.findViewById(R.id.bt_travel);
        all_for_home = view.findViewById(R.id.bt_all_for_home);
        entertainment = view.findViewById(R.id.bt_entertainment);
        food_delivery = view.findViewById(R.id.bt_food_delivery);
        services = view.findViewById(R.id.bt_services);
        finance = view.findViewById(R.id.bt_finance);
        closing = view.findViewById(R.id.bt_closing);
        china_shop = view.findViewById(R.id.bt_china_shop);
        foreign_shop = view.findViewById(R.id.bt_foreign_shop);
        all_shops = view.findViewById(R.id.bt_all_shops);

        jewelry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Украшения и Аксесуары");
            }
        });

        beauty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Красота и Здоровье");
            }
        });

        electric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Электроника и Техника");
            }
        });

        travel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Путешествия");
            }
        });

        all_for_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Все для дома");
            }
        });

        entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Досуг и Развлечения");
            }
        });

        food_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Доставка еды");
            }
        });

        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Услуги");
            }
        });

        finance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Страхование и финансы");
            }
        });

        closing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Одежда и Обувь");
            }
        });

        china_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Китайские магазины");
            }
        });

        foreign_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Иностранные магазины");
            }
        });

        all_shops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCreateAllShopsFragment("Все магазины");
            }
        });


        NavigationView navigationView1 = (NavigationView) view.findViewById(R.id.nv);
        NavigationView navigationView2 = (NavigationView) view.findViewById(R.id.nv_right);
        navigationView1.setNavigationItemSelectedListener(this);
        navigationView2.setNavigationItemSelectedListener(this);
        return view;
    }

    @Override
    public boolean onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.activity_main);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return true;
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String text = "";
        if (id == R.id.nav_describe) {
            mCallbacks.onCreateDescribeFragment();
        } else if (id == R.id.nav_referal_program) {
            mCallbacks.onCreateReferalProgramFragment();
        } else if (id == R.id.nav_rule) {
            mCallbacks.onCreateRuleFragment();
        } else if (id == R.id.nav_agreement) {
            mCallbacks.onCreateAgreementFragment();
        } else if (id == R.id.nav_questions) {
            mCallbacks.onCreateAskFragment();
        } else if (id == R.id.nav_support) {
            mCallbacks.onSupportFragment();
        }
        else if (id == R.id.nav_facebook) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://www.facebook.com/Tree-Cash-400476877224582"));
            startActivity(i);
        } else if (id == R.id.nav_instagram) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://www.instagram.com/treecash.ua/"));
            startActivity(i);
        }
         else if (id == R.id.nav_profile) {
            mCallbacks.onCreateProfileFragment();
        }
        else if (id == R.id.nav_orders) {
            mCallbacks.onCreateMyOrdersFragment();
        }
        else if (id == R.id.nav_parole) {
            mCallbacks.onCreateChangePasswordFragment();
        }
        else if (id == R.id.nav_edit_date) {
            mCallbacks.onCreateEditDataFragment();
        }
        else if (id == R.id.nav_money) {
            mCallbacks.onCreateWithdrawalOfFundsFragment();
        }
        else if (id == R.id.nav_exit) {
            mCallbacks.onCreateEnterFragment();
        }
        else if (id == R.id.nav_about_us) {
            mCallbacks.onCreateReviewsFragment();
        }
        else if (id == R.id.nav_all_shops) {
            mCallbacks.onCreateAllShopsFragment("Все магазины");
        }
        else if (id == R.id.nav_blog) {
            mCallbacks.onCreateBlogFragment();
        }
        else if (id == R.id.nav_favorites) {
            mCallbacks.onCreateAllShopsFragment("Мои магазины");
        }
//        Toast.makeText(getActivity(), "You have chosen " + text, Toast.LENGTH_LONG).show();
        DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.activity_main);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

}
