package com.sashashtmv.treecash.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.sashashtmv.treecash.MainActivity;
import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.api.APIClient;
import com.sashashtmv.treecash.api.APIInterface;
import com.sashashtmv.treecash.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {


    private static Context mContext;
    private EditText name;
    private EditText mail;
    private EditText question;
    private Button sendQuestion;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;

    public SupportFragment() {
        // Required empty public constructor
    }

    public static SupportFragment newInstance(Context context) {
        SupportFragment fragment = new SupportFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        name = view.findViewById(R.id.et_name);
        mail = view.findViewById(R.id.et_mail);
        question = view.findViewById(R.id.et_message);
        sendQuestion = view.findViewById(R.id.bt_send);

        sendQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onButtonPressed();
                ((MainActivity) mContext).onBackPressed();
            }
        });


        return view;
    }

    private void sendQuestion(String text) {
        Log.i("TAG", "sendReview: - " + text);


        try {
            String token = mPreferenceHelper.getString("access_token");
            Call<Object> call1 = apiInterface.sendQuestion("Bearer " + token, text);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: message sent" + response.code() + response.body() + "; " + token);
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 0) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
//                            Toast.makeText(mContext, "Up button", Toast.LENGTH_LONG).show();
//                            Log.i(TAG, "onClick: return in enterfragment - " + true);
//                            Fragment fragment = mFragmentManager.findFragmentByTag("enterfragment");
//                            if (fragment != null){
//                                fragment.getFragmentManager().beginTransaction().replace(R.id.container, fragment)
//                                        .addToBackStack(null)
//                                        .commit();
//                            }
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                Log.i(TAG, "showAlert: - " + " false");
                e.printStackTrace();
            }

        }
    }

    public boolean checkValidation() {

        if (name.getText().toString().equals("") || mail.getText().toString().equals("")) {
            SupportFragment.CommonMethod.showAlert("Заполните все поля", (MainActivity) mContext);
            return false;
        }
        if (question.getText().toString().equals("")){
            SupportFragment.CommonMethod.showAlert("Напишите Ваш вопрос", (MainActivity) mContext);
            return false;
        }
        return true;
    }


    public void onButtonPressed() {
        if (!SupportFragment.CommonMethod.isNetworkAvailable(mContext)){
            SupportFragment.CommonMethod.showAlert("Подключите интернет", (MainActivity) mContext);
        }
        else if (checkValidation()) {
            sendQuestion(name.getText() + "\n" + mail.getText() + "\n" + question.getText());
            SupportFragment.CommonMethod.showAlert("Ваш вопрос направлен в службу поддержки.", (MainActivity) mContext);
        }
    }

}
