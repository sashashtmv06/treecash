package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.adapters.OrdersAdapter;
import com.sashashtmv.treecash.adapters.ShopListAdapter;
import com.sashashtmv.treecash.model.Order;
import com.sashashtmv.treecash.model.Shop;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrdersFragment extends Fragment {


    private static Context mContext;
    private static OrdersAdapter mAdapter;
    private static List<Order> mList;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;

    public MyOrdersFragment() {
        // Required empty public constructor
    }

    public static MyOrdersFragment newInstance(List<Order> list, Context context) {
        MyOrdersFragment fragment = new MyOrdersFragment();
        mContext = context;
        mList = list;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_orders, container, false);

        mAdapter = new OrdersAdapter(getActivity(), mList);

        mRecyclerView = view.findViewById(R.id.rv_my_orders);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        return view;
    }

}
