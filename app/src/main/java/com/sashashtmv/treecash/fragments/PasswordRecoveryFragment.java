package com.sashashtmv.treecash.fragments;


import android.app.Activity;
import android.content.Context;

import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import android.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.treecash.MainActivity;
import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.api.APIClient;
import com.sashashtmv.treecash.api.APIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import android.app.FragmentManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordRecoveryFragment extends Fragment {


    private static Context mContext;
    private EditText email;
    private APIInterface apiInterface;
    private Button sendEmail;
    static FragmentManager mFragmentManager;

    public PasswordRecoveryFragment() {
        // Required empty public constructor
    }
    public static PasswordRecoveryFragment newInstance(Context context) {
        PasswordRecoveryFragment fragment = new PasswordRecoveryFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_recovery, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        email = view.findViewById(R.id.et_email);
        sendEmail = view.findViewById(R.id.bt_next);
        mFragmentManager = ((MainActivity)mContext).getFragmentManager();

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void loginRetrofit2Api(String email) {
        Log.i("TAG", "loginRetrofit2Api: - " + email);


        try {

            Call<Object> call1 = apiInterface.resetPassword(email);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: resetPassword" + response.code() + response.body());
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code()==0) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
//
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                    } else {
//                        Toast.makeText(mContext, "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        } catch (Exception e) {

        }
    }


    public boolean checkValidation() {

        if (email.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Введите адрес своей почты", (MainActivity) mContext);
            return false;
        }
        return true;
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Toast.makeText(mContext, "Up button", Toast.LENGTH_LONG).show();
                            Log.i(TAG, "onClick: return in enterfragment - " + true);
                            Fragment fragment = mFragmentManager.findFragmentByTag("enterfragment");
                            if (fragment != null){
                                fragment.getFragmentManager().beginTransaction().replace(R.id.container, fragment)
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                Log.i(TAG, "showAlert: - " + " false");
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (checkValidation() && CommonMethod.isNetworkAvailable(mContext)) {
            loginRetrofit2Api(email.getText().toString());
            CommonMethod.showAlert("Проверьте вашу почту", (MainActivity) mContext);
        }else CommonMethod.showAlert("Подключите интернет", (MainActivity) mContext);
    }

}
