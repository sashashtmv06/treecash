package com.sashashtmv.treecash.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.gson.Gson;
import com.sashashtmv.treecash.MainActivity;
import com.sashashtmv.treecash.MyApp;
import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.api.APIClient;
import com.sashashtmv.treecash.api.APIInterface;
import com.sashashtmv.treecash.model.PreferenceHelper;
import com.sashashtmv.treecash.model.User;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

public class EnterFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener{


    private String telephoneMail;
    private String parole;
    private EditText mTelephone;
    private EditText mPassword;
    private Button mNext;
    private Button mRegistration;
    private TextView recoveryPassword;

    private ImageView vk;
    private ImageView facebook;
    private ImageView google;

    private APIInterface apiInterface;
    private GoogleSignInOptions mGoogleSignInOptions;
    private PreferenceHelper mPreferenceHelper;
    static Context mContext;
    public static final String[] VK_SCOPES = new String[]{
            VKScope.FRIENDS,
            VKScope.NOTIFICATIONS,
            VKScope.OFFLINE,
            VKScope.STATUS,
            VKScope.STATS,
            VKScope.PHOTOS
    };

    private OnFragmentEnterListener mListener;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;

    public EnterFragment() {
        // Required empty public constructor
    }


    public static EnterFragment newInstance(Context context) {
        EnterFragment fragment = new EnterFragment();
        mContext = context;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter, container, false);


        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        mTelephone = view.findViewById(R.id.et_email);
        mPassword = view.findViewById(R.id.et_parole);
        mNext = view.findViewById(R.id.bt_next);
        mRegistration = view.findViewById(R.id.bt_registration);
        recoveryPassword = view.findViewById(R.id.tv_recovery);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        vk = view.findViewById(R.id.iv_vk);
        facebook = view.findViewById(R.id.iv_facebook);
//        facebook.setReadPermissions("email");
        // If using in a fragment
//        facebook.setFragment(this);
        google = view.findViewById(R.id.iv_google);
        Log.i(TAG, "login with google: 1- " + mContext);

        vk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                VKSdk.login((MainActivity) mContext, VK_SCOPES);
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "onSuccessEnterFragment: " + loginResult.getAccessToken().getToken());
                getToken(loginResult.getAccessToken().getToken(), "facebook");
//                int userId = Integer.parseInt(loginResult.getAccessToken().getUserId());
//                retrieveFbUserProfile(loginResult.getAccessToken());
//                if(userId > 0 ){
//                    ((MainActivity)mContext).onFragmentStart();
//                }
                // App code
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "onCancelEnterFragment: " + "cancel");
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Log.i(TAG, "onError: " + "fail");// App code
            }
        });
                Log.i(TAG, "onClick: facebook");
                LoginManager.getInstance().logInWithReadPermissions(EnterFragment.this, Arrays.asList("public_profile", "email"));


            }
        });


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithGoogle();
            }
        });


        mNext.setOnClickListener(mOnEnterClickListener);
        mRegistration.setOnClickListener(mOnRegistrationListener);
        recoveryPassword.setOnClickListener(mOnRecoveryListener);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mGoogleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.ClientID))
                .requestEmail()
//                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
//                .requestScopes(new Scope(Scopes.PLUS_ME))
                .build();

        try {
            // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
            if(mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder((MainActivity) mContext)
                        .enableAutoManage((MainActivity) mContext, this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSignInOptions)
                        .build();
            }
        Log.i(TAG, "login with google: " + mGoogleApiClient + " context - " + mContext);
        }catch (Exception e){
            Log.i(TAG, "login with google: error - " + mGoogleApiClient + " context - " + mContext);
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage((MainActivity)mContext);
        mGoogleApiClient.disconnect();
    }

    private void loginWithGoogle() {
        Log.i(TAG, "onActivityResult: - " + true);
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((MainActivity)mContext).startActivityForResult(signInIntent, MainActivity.ACTIVITY_RESULT_AUTH);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onResultEnterFragment: " + " - " + (resultCode == RESULT_OK));
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }


    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            onButtonPressed();
//
            if (checkValidation()) {
                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
                    loginRetrofit2Api(mTelephone.getText().toString(), mPassword.getText().toString());
                else
                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
            }
            else
                showMessage(R.string.login_input_error);
        }
    };

    private View.OnClickListener mOnRegistrationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.onFragmentRegistration();
//            if (checkValidation()) {
//                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
//                    loginRetrofit2Api(mTelephone.getText().toString(), mPassword.getText().toString());
//                else
//                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
//            }
//            else
//                showMessage(R.string.login_input_error);
        }
    };

    private View.OnClickListener mOnRecoveryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.onRecoveryPasswordFragment();
//            if (checkValidation()) {
//                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
//                    loginRetrofit2Api(mTelephone.getText().toString(), mPassword.getText().toString());
//                else
//                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
//            }
//            else
//                showMessage(R.string.login_input_error);
        }
    };
//
    private void showMessage(@StringRes int string) {
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();
    }

    private void loginRetrofit2Api(String username, String password) {
        Log.i("TAG", "loginRetrofit2Api: - " + username + " " + password);
        List<String> list = new ArrayList<>();
        list.add(username);
        list.add(password);

        try {

            JSONObject paramObject = new JSONObject();
            paramObject.put("username", username);
            paramObject.put("password", password);

            Call<Object> call1 = apiInterface.createValidation(paramObject.toString());
//            Call<Object> call1 = apiInterface.createValidation(username, password);
    Log.i(TAG, "onResponse: 111 " + true);
            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: 111 " + response.code() + response.body());
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
//                            boolean access = object.getBoolean("status");
                            String token = object.getString("token");
                            Log.i(TAG, "onResponse: 111 " + token);
                            if (token != null && token.length() > 0) {
                                mPreferenceHelper.putString("access_token", token);
                                onButtonPressed();
                            }
//                            Log.e("TAG", "response 33: " + token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }
//                    else onButtonPressed();

                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        } catch (Exception e) {

        }
    }

    public void getToken(String access_token, String name_of_social) {
        Log.i("TAG", "loginRetrofit2Api: - " + access_token);

        try {
            Call<Object> call1 = apiInterface.getToken(name_of_social, access_token);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: token" + response.code() + response.body());
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
//                            boolean access = object.getBoolean("status");
                            String token = object.getString("token");//
                            Log.i(TAG, "onResponse: " + token);
                            if (token != null && token.length() > 0) {
                                mPreferenceHelper.putString("access_token", token);
                                onButtonPressed();
                            }
//                            Log.e("TAG", "response 33: " + token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                    } else {
//                        Toast.makeText(mContext, "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }
                    else onButtonPressed();


                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        } catch (Exception e) {

        }
    }


    public boolean checkValidation() {

        if (mPassword.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mTelephone.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Телефон нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mPassword.getText().toString().trim().length() < 7) {
            EnterFragment.CommonMethod.showAlert("Пароль не может быть меньше 7 символов", (MainActivity) mContext);
            return false;
        }
            return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            try {
                builder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentStart();
            hideKeyboard(getActivity());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentEnterListener) {
            mListener = (OnFragmentEnterListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentEnterListener {

        void onFragmentStart();
        void onFragmentRegistration();
        void onRecoveryPasswordFragment();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
