package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.treecash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DescribeFragment extends Fragment {
    static Context mContext;


    public DescribeFragment() {
        // Required empty public constructor
    }

    public static DescribeFragment newInstance(Context context) {
        mContext = context;
        DescribeFragment fragment = new DescribeFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_describe, container, false);
    }

}
