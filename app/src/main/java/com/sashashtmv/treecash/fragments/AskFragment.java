package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;
import android.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.sashashtmv.treecash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AskFragment extends Fragment {


    private static Context mContext;
    private TextView ask25;
    private SupportFragment supportFragment;
    private FragmentManager mFragmentManager;

    public AskFragment() {
        // Required empty public constructor
    }

    public static AskFragment newInstance(Context context) {
        AskFragment fragment = new AskFragment();
        mContext = context;


        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ask, container, false);
        ask25 = view.findViewById(R.id.tv_describe_ask25);
        final String str = "Оставить идею можно здесь!";
        mFragmentManager = getActivity().getFragmentManager();
        AgreementFragment.ClickSpan.clickify(ask25, str, new AgreementFragment.ClickSpan.OnClickListener() {
            @Override
            public void onClick() {
                supportFragment = SupportFragment.newInstance(mContext);
                mFragmentManager.beginTransaction()
                        .replace(R.id.container, supportFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

}
