package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.treecash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReferalProgramFragment extends Fragment {


    private static Context mContext;

    public ReferalProgramFragment() {
        // Required empty public constructor
    }
    public static ReferalProgramFragment newInstance(Context context) {
        mContext = context;
        ReferalProgramFragment fragment = new ReferalProgramFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_referal_program, container, false);
    }

}
