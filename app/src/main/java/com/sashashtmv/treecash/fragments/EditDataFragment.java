package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sashashtmv.treecash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDataFragment extends Fragment {


    private static Context mContext;


    public EditDataFragment() {
        // Required empty public constructor
    }
    public static EditDataFragment newInstance(Context context) {
        EditDataFragment fragment = new EditDataFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_edit_data, container, false);


        return view;
    }

}
