package com.sashashtmv.treecash.fragments;

public interface IOnBackPressed {
    boolean onBackPressed();
}
