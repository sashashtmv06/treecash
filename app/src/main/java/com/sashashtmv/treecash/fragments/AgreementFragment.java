package com.sashashtmv.treecash.fragments;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.app.Fragment;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sashashtmv.treecash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgreementFragment extends Fragment {


    private static Context mContext;
    private TextView text1;
    private TextView text2;

    public AgreementFragment() {
        // Required empty public constructor
    }

    public static AgreementFragment newInstance(Context context) {
        AgreementFragment fragment = new AgreementFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agreement, container, false);
        text1 = view.findViewById(R.id.tv_describe_terms);
        text2 = view.findViewById(R.id.tv_describe_title1);
        final String str = "https://treecash.shop/";
        ClickSpan.clickify(text1, str,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(str));
                startActivity(i);
            }
        });
        ClickSpan.clickify(text2, str,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(str));
                startActivity(i);
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    public static class ClickSpan extends ClickableSpan {

        private OnClickListener mListener;

        public ClickSpan(OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View widget) {
            if (mListener != null) mListener.onClick();
        }

        public interface OnClickListener {
            void onClick();
        }
    public static void clickify(TextView view, final String clickableText,
                                final ClickSpan.OnClickListener listener) {

        CharSequence text = view.getText();
        String string = text.toString();
        ClickSpan span = new ClickSpan(listener);

        int start = string.indexOf(clickableText);
        int end = start + clickableText.length();
        if (start == -1) return;

        if (text instanceof Spannable) {
            ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            SpannableString s = SpannableString.valueOf(text);
            s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(s);
        }

        MovementMethod m = view.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            view.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }
    }


}
