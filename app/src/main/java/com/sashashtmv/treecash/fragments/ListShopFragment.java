package com.sashashtmv.treecash.fragments;


import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.adapters.ShopListAdapter;
import com.sashashtmv.treecash.model.Shop;

import java.util.ArrayList;
import java.util.List;

public class ListShopFragment extends Fragment {

    static String mTitle;

    private static ShopListAdapter mAdapter;
    private static List<Shop> mList;
    private SearchView mSearchView;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    TextView tvTitle;
    Toolbar toolbar;


    public ListShopFragment() {
        // Required empty public constructor
    }

    public static ListShopFragment newInstance(String title, List<Shop> list) {
        ListShopFragment fragment = new ListShopFragment();
        mTitle = title;
        mList = list;

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_shop, container, false);
        // Inflate the layout for this fragment
        toolbar = view.findViewById(R.id.toolbar);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (toolbar != null) {
            toolbar.setTitle(R.string.title);
            toolbar.setTitleTextColor(getResources().getColor(R.color.design_default_color_primary_dark));

            activity.setSupportActionBar(toolbar);
            activity.getSupportActionBar().setTitle("My title");
        }
        mAdapter = new ShopListAdapter(getActivity(), mList);
        mSearchView = view.findViewById(R.id.search_view);
        tvTitle = view.findViewById(R.id.tv_title);
        tvTitle.setText(mTitle);

        mRecyclerView = view.findViewById(R.id.rvShopList);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                findOrder(newText);

                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tvTitle.setText(mTitle);
                return false;
            }
        });
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvTitle.setText("");

            }

        });
        return view;
    }

    public void findOrder(String title) {
        //будем удалять все элементы из списка заказов
        //checkAdapter();
        if(mAdapter != null && mList != null) {
            mAdapter.removeAllItems();
            List<Shop> shops = new ArrayList<>();
            for (int i = 0; i < mList.size(); i++) {
                if (mList.get(i).getNameShop().toLowerCase().contains(title.toLowerCase())) {
                    shops.add(mList.get(i));
                }
                mAdapter.addShops(shops);

            }
        }

    }

}
