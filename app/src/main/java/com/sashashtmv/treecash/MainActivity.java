package com.sashashtmv.treecash;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.app.FragmentManager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.sashashtmv.treecash.adapters.ReviewListAdapter;
import com.sashashtmv.treecash.api.APIClient;
import com.sashashtmv.treecash.api.APIInterface;
import com.sashashtmv.treecash.fragments.AgreementFragment;
import com.sashashtmv.treecash.fragments.AskFragment;
import com.sashashtmv.treecash.fragments.BlogFragment;
import com.sashashtmv.treecash.fragments.ChangePasswordFragment;
import com.sashashtmv.treecash.fragments.DescribeFragment;
import com.sashashtmv.treecash.fragments.EditDataFragment;
import com.sashashtmv.treecash.fragments.EnterFragment;
import com.sashashtmv.treecash.fragments.IOnBackPressed;
import com.sashashtmv.treecash.fragments.ListShopFragment;
import com.sashashtmv.treecash.fragments.MyOrdersFragment;
import com.sashashtmv.treecash.fragments.PasswordRecoveryFragment;
import com.sashashtmv.treecash.fragments.ProfileFragment;
import com.sashashtmv.treecash.fragments.ReferalProgramFragment;
import com.sashashtmv.treecash.fragments.RegistrationFragment;
import com.sashashtmv.treecash.fragments.ReviewsFragment;
import com.sashashtmv.treecash.fragments.RulesFragment;
import com.sashashtmv.treecash.fragments.StartFragment;
import com.sashashtmv.treecash.fragments.SupportFragment;
import com.sashashtmv.treecash.fragments.WithdrawalOfFundsFragment;
import com.sashashtmv.treecash.model.Article;
import com.sashashtmv.treecash.model.Order;
import com.sashashtmv.treecash.model.PreferenceHelper;
import com.sashashtmv.treecash.model.Review;
import com.sashashtmv.treecash.model.Shop;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


//import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;

import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.vk.sdk.dialogs.VKOpenAuthDialog.VK_EXTRA_TOKEN_DATA;

public class MainActivity extends AppCompatActivity implements StartFragment.Callbacks, EnterFragment.OnFragmentEnterListener,
        RegistrationFragment.OnFragmentRegistrationListener {

    public static final int ACTIVITY_RESULT_AUTH = 5;
    FragmentManager mFragmentManager;
    StartFragment startFragment;
    EnterFragment enterFragment;
    RegistrationFragment registrationFragment;
    DescribeFragment describeFragment;
    ReferalProgramFragment referalProgramFragment;
    AgreementFragment agreementFragment;
    AskFragment askFragment;
    SupportFragment supportFragment;
    PasswordRecoveryFragment passwordRecoveryFragment;
    ProfileFragment profileFragment;
    MyOrdersFragment myOrdersFragment;
    ChangePasswordFragment changePasswordFragment;
    EditDataFragment editDataFragment;
    WithdrawalOfFundsFragment withdrawalOfFundsFragment;
    ReviewsFragment reviewsFragment;
    ListShopFragment listShopFragment;
    BlogFragment blogFragment;
    private CallbackManager callbackManager;
    private RulesFragment ruleFragment;
    private List<Shop> list;
    private List<Article> listArticle;
    private List<Order> listOrder;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callbackManager = CallbackManager.Factory.create();
        list = new ArrayList<>();
        Log.i(TAG, "onCreate: list - " + "true");
        listArticle = new ArrayList<>();
        listOrder = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceHelper.getInstance().init(this);
        mPreferenceHelper = PreferenceHelper.getInstance();

        mFragmentManager = getFragmentManager();
        onCreateEnterFragment();
        getShops(mPreferenceHelper.getString("access_token"));
        getArticles(mPreferenceHelper.getString("access_token"));
        getOrders(mPreferenceHelper.getString("access_token"));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult: - " + "resultCode - " + resultCode + ", " + requestCode);
        VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                String token = res.accessToken; //getting our token here.
                if (token != null) {
                    String email = VKSdk.getAccessToken().email;
                    String userId = VKSdk.getAccessToken().userId;
                    Log.i(TAG, "onResult: vk - " + token + ", mail - " + res.email + ", userID - " + res.userId + ", " + userId + ", " + res.secret);

                    VKApi.users().get().executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            VKApiUser user = ((VKList<VKApiUser>) response.parsedModel).get(0);
                            Log.d("User name", user.first_name + " " + user.last_name);
                        }
                    });
                    enterFragment.getToken(token, "vkontakte");
                }
            }

            @Override
            public void onError(VKError error) {
//                Toast.makeText(SocialNetworkChooseActivity.this,
//                        "User didn't pass Authorization", Toast.LENGTH_SHORT).show();
            }
        });

        if (requestCode == ACTIVITY_RESULT_AUTH) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.i(TAG, "onResponse: google token - " + result.getStatus());
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                String authCode = acct.getServerAuthCode();
                getAccessToken(authCode);
            }
        }
    }


    public void getAccessToken(String authCode) {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("client_id", getString(R.string.ClientID))
                .add("client_secret", getString(R.string.ClientSecret))
                .add("code", authCode)
                .build();
        final Request request = new Request.Builder()
                .url("https://www.googleapis.com/oauth2/v4/token")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                Log.i(TAG, "onResponse: google token - " + "false");
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                try {
//                Log.i(TAG, "onResponse: google token - " + response.body().string());
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String mAccessToken = jsonObject.get("access_token").toString();
                    String mTokenType = jsonObject.get("token_type").toString();
//                    String mRefreshToken = jsonObject.get("refresh_token").toString();
            enterFragment.getToken(mAccessToken, "google");
                    Log.i(TAG, "onResponse: google token - " + mAccessToken);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if (!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreateAllShopsFragment(String title) {
        List<Shop> shops = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (title.toLowerCase().equals(list.get(i).getCategoryName().toLowerCase())) {
                shops.add(list.get(i));
            }
            if(title.equals("Мои магазины") && list.get(i).isLove()){
                shops.add(list.get(i));
            }
        }
        if (title.equals("Все магазины")) {
            shops = list;
        }
        listShopFragment = ListShopFragment.newInstance(title, shops);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, listShopFragment, "allShops")
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onCreateBlogFragment() {
        blogFragment = BlogFragment.newInstance(this, listArticle);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, blogFragment, "allArticles")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateReviewsFragment() {
        reviewsFragment = ReviewsFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, reviewsFragment, "reviews")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateEnterFragment() {
        enterFragment = EnterFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, enterFragment, "enterfragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateDescribeFragment() {
        describeFragment = DescribeFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, describeFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateReferalProgramFragment() {
        referalProgramFragment = ReferalProgramFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, referalProgramFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateRuleFragment() {
        ruleFragment = RulesFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, ruleFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateAgreementFragment() {
        agreementFragment = AgreementFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, agreementFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateAskFragment() {
        askFragment = AskFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, askFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onSupportFragment() {
        supportFragment = SupportFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, supportFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFragmentStart() {
        getShops(mPreferenceHelper.getString("access_token"));
        startFragment = StartFragment.newInstance();
        mFragmentManager.beginTransaction()
                .replace(R.id.container, startFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFragmentRegistration() {
        registrationFragment = RegistrationFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, registrationFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRecoveryPasswordFragment() {
        passwordRecoveryFragment = PasswordRecoveryFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, passwordRecoveryFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateProfileFragment() {
        profileFragment = ProfileFragment.newInstance(this, mPreferenceHelper.getString("ready_to_pay"), mPreferenceHelper.getString("pending_to_pay"));
        mFragmentManager.beginTransaction()
                .replace(R.id.container, profileFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateMyOrdersFragment() {
        myOrdersFragment = MyOrdersFragment.newInstance(listOrder, this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, myOrdersFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateChangePasswordFragment() {
        changePasswordFragment = ChangePasswordFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, changePasswordFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateEditDataFragment() {
        editDataFragment = EditDataFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, editDataFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateWithdrawalOfFundsFragment() {
        withdrawalOfFundsFragment = WithdrawalOfFundsFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, withdrawalOfFundsFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFragmentInteraction() {
        startFragment = StartFragment.newInstance();
        mFragmentManager.beginTransaction()
                .replace(R.id.container, startFragment)
                .addToBackStack(null)
                .commit();
    }

    private void getShops(String token) {
        list = new ArrayList<>();
        Log.i("TAG", "getShops: - " + token);
        try {
            Call<Object> call1 = apiInterface.getShops("Bearer " + token);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: shops get" + response.code() + response.body());
                    JSONObject object = null;

                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("shops");

                            for (int i = 0; i < array.length(); i++) {
                                String temp = array.getString(i).toString();
                                object = new JSONObject(temp);
                                String shopName = object.getString("name");
                                int categoryId = object.getInt("category_id");
                                String categoryName = object.getString("category_name");
                                String imageUrl = object.getString("image");
                                URL url = new URL("https://treecash.shop" + imageUrl);
                                String shopUrl = object.getString("link");
                                String cashback = object.getString("cashback");
                                            Log.i(TAG, "parsing cashback: - " + shopName + ", " + cashback);
                                Log.i(TAG, "onResponse: 222 " + cashback);
                                List<String> parseCashback = new ArrayList<>();

                                if (cashback.contains("<table")) {
                                    for(int k = 0; k < cashback.length(); k++){
                                        String str = "";
                                        if(cashback.charAt(k) == '>'){
                                            int index = cashback.substring(k).indexOf("<");

                                            if(index > -1)
                                                str = cashback.substring(k+1, cashback.substring(k).indexOf("<") + k);
                                            Log.i(TAG, "parsing cashback: - 1 - " + str);
                                            str = str.trim();
                                            str = str.replace("&amp", "");
                                        }
                                        if(str.length() > 0) parseCashback.add(str);
                                    }
                                    for (int j = 0; j < parseCashback.size(); j++) {
                                        Log.i(TAG, "parsing cashback: - " + parseCashback.get(j));
                                    }
                                } else {
                                    if(cashback.length() > 100) {
                                        for (int k = 0; k < cashback.length(); k++) {
                                            String str = "";
                                            if (cashback.charAt(k) == '>') {
                                                int index = cashback.substring(k).indexOf("<");

                                                if (index > -1)
                                                    str = cashback.substring(k + 1, cashback.substring(k).indexOf("<") + k);
                                                Log.i(TAG, "parsing cashback: - 1 - " + str);
                                                str = str.trim();
                                                str = str.replace("&amp", "");
                                            }
                                            if (str.length() > 0) parseCashback.add(str);
                                        }
                                    }else parseCashback.add(cashback);
                                }
                                Log.i(TAG, "onResponse: 222  ------  " + parseCashback);
                                Shop shop = new Shop(shopName, shopUrl, url, parseCashback, categoryName, categoryId, false);
                                if(mPreferenceHelper.getString(shopName).equals("true")){
                                    shop.setLove(true);
                                }
                                list.add(shop);
                                Log.i(TAG, "onResponse: shop - " + shopName + "; " + categoryName + "; " + parseCashback);

                            }
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + t.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getArticles(String token) {
        Log.i("TAG", "getShops: - " + token);
        try {
            Call<Object> call1 = apiInterface.getArticles("Bearer " + token, "blog");
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: articles get" + response.code() + response.body());
                    JSONObject object = null;

                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("articles");
//
                            for (int i = 0; i < array.length(); i++) {
                                String temp = array.getString(i).toString();
                                object = new JSONObject(temp);
                                String title = object.getString("title");
                                String text = object.getString("text");
                                String imageUrl = object.getString("image");

                                text = text.replace("><", "");
                                String[] mas = text.split("\n");
                                text = "";
//                                text = text.substring(text.indexOf(">"), (text.substring(text.indexOf(">")).indexOf("<")));
                                for (int j = 0; j < mas.length; j++) {
                                    String str = mas[j].replace("><", "");
                                    if (!str.contains("<div") & str.contains("<") & str.contains(">") & str.indexOf(">") < str.lastIndexOf("<")) {
//                                        str = str.substring(str.indexOf(">"), (str.substring(str.indexOf(">")).indexOf("<")));
                                        str = str.substring(str.indexOf(">") + 1, (str.lastIndexOf("<")));
                                        str = str.replace("</strong>", "");
                                        text = text + str + "\n";
                                        Log.i(TAG, "onResponse: article - " + str);
                                    }

                                }
                                URL url;
                                if (imageUrl.startsWith("/")) {
                                    url = new URL("https://tc.orido.org" + imageUrl);

                                } else url = new URL(imageUrl);

                                listArticle.add(new Article(title, text, url));

                            }
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + t.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getOrders(String token) {

        Log.i("TAG", "getShops: - " + token);
        try {
            Call<Object> call1 = apiInterface.getOrders("Bearer " + token);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: articles get" + response.code() + response.body());
                    JSONObject object = null;

                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("orders");
//
                            for (int i = 0; i < array.length(); i++) {
                                String temp = array.getString(i);
                                object = new JSONObject(temp);
                                String id = object.getString("id");
                                String sum = object.getString("summ");
                                String currency = object.getString("currency");
                                String storeName = object.getString("store");
                                String date = object.getString("date");
                                String status = object.getString("status");

                                Log.i(TAG, "onResponse: getOrders - " + id);
                                listOrder.add(new Order(date, id, storeName, sum, status, currency));

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + t.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getBalance() {
        String token = mPreferenceHelper.getString("access_token");
        try {
            Call<Object> call1 = apiInterface.getBalance("Bearer " + token);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: articles get" + response.code() + response.body());
                    JSONObject object = null;

                    try {
                        if (response.isSuccessful() && response.body() != null) {
//                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("orders");//

                                object = new JSONObject(response.body().toString());
                                String status = object.getString("status");
                                String ready_to_pay = object.getString("ready_to_pay");
                                String pending_to_pay = object.getString("pending_to_pay");
                                if(status.equals("true")){
                                    mPreferenceHelper.putString("ready_to_pay", ready_to_pay);
                                    mPreferenceHelper.putString("pending_to_pay", pending_to_pay);
                                }

                                Log.i(TAG, "onResponse: getBalance - " + pending_to_pay + "; " + ready_to_pay);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + t.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void withdrawals(String sum, String type, String number) {
        String token = mPreferenceHelper.getString("access_token");
        try {
            Call<Object> call1 = apiInterface.withdrawals("Bearer " + token, sum, type, number);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: withdrawals - " + response.code() + response.body());
                    JSONObject object = null;

                    try {
                        if (response.isSuccessful() && response.body() != null) {
//                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("orders");//

                            object = new JSONObject(response.body().toString());
                            String status = object.getString("status");
//                            String ready_to_pay = object.getString("ready_to_pay");
//                            String pending_to_pay = object.getString("pending_to_pay");
//                            if(status.equals("true")){
//                                mPreferenceHelper.putString("ready_to_pay", ready_to_pay);
//                                mPreferenceHelper.putString("pending_to_pay", pending_to_pay);
//                            }
//
//                            Log.i(TAG, "onResponse: getBalance - " + pending_to_pay + "; " + ready_to_pay);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + t.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
//
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                Log.i(TAG, "showAlert: - " + " false");
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (!ReviewsFragment.CommonMethod.isNetworkAvailable(this)) {
            ReviewsFragment.CommonMethod.showAlert("Подключите интернет", this);
        }

    }
}
