package com.sashashtmv.treecash.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    //PreferenceHelper mPreferenceHelper = PreferenceHelper.getInstance();
//    @Headers("Content-Type: application/json")

    @FormUrlEncoded
    @POST("register")
    Call<Object> createUser(
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("email") String email);

//    Call<AddEmailResult> addEmail(@Body List<String> emails);
//    Call getData(@Query("phone") String phone,
//                           @Query("password") String password,
//                           @Query("name") String name,
//                           @Query("surname") String surname,
//                           @Query("email") String email);
//    Call<LoginResponse> createUser(@Body LoginResponse login);
//    Call<Object> createUser(@Body String body);
//    Call<ResponseData> createUser(@Body RequestBody body);

    //
    @Headers("Content-Type: application/json")
    @POST("login_check")
    Call<Object> createValidation(@Body String token);

    @GET("reset_password/{email}")
    Call<Object> resetPassword(@Path("email") String email);

//    @Headers("Authorization") String token
    @FormUrlEncoded
    @POST("reviews/add")
    Call<Object> sendReview(@Header("Authorization") String tokenString,  @Field("text") String text);

    @GET("reviews/list")
    Call<Object> getReviews(@Header("Authorization") String tokenString);


    @POST("shops/list")
    Call<Object> getShops(@Header("Authorization") String tokenString);

    @FormUrlEncoded
    @POST("articles/list")
    Call<Object> getArticles(@Header("Authorization") String tokenString,  @Field("type") String text);

    @FormUrlEncoded
    @POST("support/new")
    Call<Object> sendQuestion(@Header("Authorization") String tokenString,  @Field("question") String text);

    @GET("orders/list")
    Call<Object> getOrders(@Header("Authorization") String tokenString);

    @GET("orders/balance")
    Call<Object> getBalance(@Header("Authorization") String tokenString);

    @FormUrlEncoded
    @POST("orders/withdraw")
    Call<Object> withdrawals(@Header("Authorization") String tokenString,
            @Field("sumToWithdraw") String sumToWithdraw,
            @Field("type") String type,
            @Field("destination") String destination);

    @FormUrlEncoded
    @POST("login_social/{name_of_social}")
    Call<Object> getToken(@Path(value = "name_of_social", encoded = true) String name_of_social, @Field("access_token") String access_token);
//    Call<Object> getShops(@Header("Authorization") String tokenString,  @Query("category_id") String text);

//    @Headers("Content-Type: application/json")
//    @POST("login_check")
//    Call<Object> createValidation(
//            @Field("username") String name,
//            @Field("password") String password
//            );

}
