package com.sashashtmv.treecash.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.treecash.MainActivity;
import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.fragments.ArticleFragment;
import com.sashashtmv.treecash.fragments.BlogFragment;
import com.sashashtmv.treecash.model.Article;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder> {

    private List<Article> articleList;
    protected Article item;
    private Context mContext;
    private ArticleFragment articleFragment;
    private FragmentManager mFragmentManager;
    Bitmap articleImage;

    public ArticlesAdapter(Context context, List<Article> list) {
        mContext = context;
        articleList = list;
        mFragmentManager = ((MainActivity)context).getFragmentManager();
    }

    @NonNull
    @Override
    public ArticlesAdapter.ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ArticleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_article, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlesAdapter.ArticleViewHolder holder, int position) {
        item = articleList.get(position);
        Glide.with(mContext)
                .load(String.valueOf(item.getImageUrl()))
                .into(holder.imageView);

        holder.titleArticles.setText(item.getTitle());
        holder.text = item.getText();
        holder.title = item.getTitle();
        holder.image = item.getImageUrl();
//        holder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                articleFragment = ArticleFragment.newInstance(item.getImageUrl(), item.getTitle(), item.getText());
//                mFragmentManager.beginTransaction()
//                        .replace(R.id.container, articleFragment, "articleFragment")
//                        .addToBackStack(null)
//                        .commit();
//            }
//        });

//        holder.image = holder.imageView.getDrawingCache();
        Log.i(TAG, "onBindViewHolder: - " + "false");

    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;
        TextView titleArticles;
        URL image;
        String title;
        String text;


        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
//            itemView.setOnClickListener(this);
            Log.i(TAG, "ArticleViewHolder: - " + "true");
            imageView = itemView.findViewById(R.id.iv_id_image);
            titleArticles = itemView.findViewById(R.id.tv_title_article);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    articleFragment = ArticleFragment.newInstance(articleList.get(position).getImageUrl(), articleList.get(position).getTitle(), articleList.get(position).getText());
                    mFragmentManager.beginTransaction()
                            .replace(R.id.container, articleFragment, "articleFragment")
                            .addToBackStack(null)
                            .commit();
                }
            });

        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "onClick: - " + "true");
            articleFragment = ArticleFragment.newInstance(image, title, text);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, articleFragment, "articleFragment")
                    .addToBackStack(null)
                    .commit();
        }
    }

//    public static class CommonMethod {
//
//        public static boolean isNetworkAvailable(Context ctx) {
//            ConnectivityManager connectivityManager
//                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//        }
//
//        public static void showAlert(String message, Activity context) {
//
//            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//            builder.setMessage(message).setCancelable(false)
//                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//
//                        }
//                    });
//            try {
//                AlertDialog alert = builder.create();
//                alert.show();
//            } catch (Exception e) {
//                Log.i(TAG, "showAlert: - " + " false");
//                e.printStackTrace();
//            }
//
//        }
//    }

}
