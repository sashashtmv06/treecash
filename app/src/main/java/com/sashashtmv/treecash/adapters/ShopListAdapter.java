package com.sashashtmv.treecash.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.treecash.MainActivity;
import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.model.PreferenceHelper;
import com.sashashtmv.treecash.model.Shop;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ShopViewHolder> {

    private List<Shop> mShops;
    protected Shop item;
    private Context mContext;
    private PreferenceHelper mPreferenceHelper;

    public ShopListAdapter(Context context, List<Shop> list) {
        mContext = context;
        mShops = list;
        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();
    }

    @NonNull
    @Override
    public ShopListAdapter.ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ShopViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_shop, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ShopListAdapter.ShopViewHolder holder, int position) {
        item = mShops.get(position);
        Log.i(TAG, "onClick: like - " + item.getImageUrl());

        Glide.with(mContext)
                .load(String.valueOf(item.getImageUrl()))
                .into(holder.imageView);

//        holder.imageView.setImageBitmap(item.getImageUrl());
        String cashback = "";
        int count = 0;
        for (int i = 0; i < item.getCashback().size(); i++) {
            if(item.getCashback().size() == 1){
                cashback = item.getCashback().get(0);
            }else {
                cashback += item.getCashback().get(i) + "  ";
                count++;
                if(count % 2 == 0){
                    cashback = cashback.trim() + "\n";
                }
            }
        }
        if(item.getCashback().size() > 1){
            holder.describeCashback.setVisibility(View.VISIBLE);
            holder.valueCashback.setVisibility(View.GONE);
        }
        holder.valueCashback.setText(cashback);
        holder.nameShop.setText(item.getNameShop());
        String url = item.getShopUrl();
        Log.i(TAG, "onClick: like - " + url);
        holder.toShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                mContext.startActivity(browserIntent);
            }
        });
        final String decscribe = cashback;
        holder.describeCashback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.CommonMethod.showAlert(decscribe, (MainActivity)mContext);
            }
        });

        if(item.isLove()){
            holder.love.setImageResource(R.drawable.love_red);
        }else {
            holder.love.setImageResource(R.drawable.love);
        }

        holder.love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.getPosition();
                item = mShops.get(pos);
                if(!item.isLove()){
                Log.i(TAG, "onClick: like - " + item.getNameShop());
                    holder.love.setImageResource(R.drawable.love_red);
                    item.setLove(true);
                    mPreferenceHelper.putString(item.getNameShop(), "true");

                }else {
                    holder.love.setImageResource(R.drawable.love);
                    item.setLove(false);
                    Log.i(TAG, "onClick: like - " + item.getNameShop());
                    mPreferenceHelper.putString(item.getNameShop(), "false");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mShops != null) {
            return mShops.size();
        }else return 0;
    }

    public class ShopViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView nameShop;
        TextView valueCashback;
        ImageView love;
        Button toShop;
        Button describeCashback;

        public ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_id_image);
            love = itemView.findViewById(R.id.iv_like);
            nameShop = itemView.findViewById(R.id.tv_name_shop);
            valueCashback = itemView.findViewById(R.id.tv_value_cashback);
            toShop = itemView.findViewById(R.id.bt_send);
            describeCashback = itemView.findViewById(R.id.bt_cashback);
        }
    }

    public void removeAllItems() {
        if (getItemCount() != 0) {
            mShops = new ArrayList<>();
//            Diagnostics.i(this, "OrderAdapter_removeAllItems success" ).append("LOG_FILE");
            notifyDataSetChanged();

        }
    }

    public void addShops(List<Shop> list) {
        mShops = list;
//        Diagnostics.i(this, "OrderAdapter_addOrdres success" ).append("LOG_FILE");
        notifyDataSetChanged();
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
//                            Toast.makeText(mContext, "Up button", Toast.LENGTH_LONG).show();
//                            Log.i(TAG, "onClick: return in enterfragment - " + true);
//                            Fragment fragment = mFragmentManager.findFragmentByTag("enterfragment");
//                            if (fragment != null){
//                                fragment.getFragmentManager().beginTransaction().replace(R.id.container, fragment)
//                                        .addToBackStack(null)
//                                        .commit();
//                            }
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                Log.i(TAG, "showAlert: - " + " false");
                e.printStackTrace();
            }

        }
    }
}
