package com.sashashtmv.treecash.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.sashashtmv.treecash.R;
import com.sashashtmv.treecash.model.Order;
import java.util.List;

import static android.content.ContentValues.TAG;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrderViewHolder> {

    private List<Order> orderList;
    protected Order item;
    private Context mContext;

    public OrdersAdapter(Context context, List<Order> list) {
        mContext = context;
        orderList = list;

    }

    @NonNull
    @Override
    public OrdersAdapter.OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_order, null));
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.OrderViewHolder holder, int position) {
        item = orderList.get(position);

        holder.tv_id.setText(item.getId());
        holder.tv_date.setText(item.getDate());
        holder.tv_nameStore.setText(item.getStoreName());
        holder.tv_currency.setText(item.getCurrency());
        holder.tv_sum.setText(item.getSum());
        holder.tv_status.setText(item.getStatus());

        Log.i(TAG, "onBindViewHolder: - orderAdapter" + "false");

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        TextView tv_id;
        TextView tv_date;
        TextView tv_nameStore;
        TextView tv_currency;
        TextView tv_sum;
        TextView tv_status;



        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            Log.i(TAG, "OrderViewHolder: - " + "true");
            tv_id = itemView.findViewById(R.id.tv_id_orders);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_nameStore = itemView.findViewById(R.id.tv_name_shop);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            tv_sum = itemView.findViewById(R.id.tv_sum);
            tv_status = itemView.findViewById(R.id.tv_status);

        }

    }

}
